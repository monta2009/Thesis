%%----------Chapter 2------------------------------------------
\chapter{Background}
\label{ch:background}

\section{Domain Name System}
Domain Name System (DNS) is the internet backbone for providing a mapping between human-readable host names and computer understandable Internet Protocol (IP) addresses.
DNS protocol is designed with a decentralized hierarchical approach.
DNS protocol uses a decentralized hierarchical approach that helped this protocol scale with the growth of Internet.
To provide this decentralization while maintaining authenticity of the results, DNS defines hierarchical divisions known as DNS zones that are distributed between name servers.
Each zone can be specified by a domain name (such as ``example.com.'') that also shows what subset of DNS queries can be answered by the name servers responsible for that zone.
Furthermore, name servers responsible for a zone can delegate the authority of each of their subdomains to other servers, thus performing ``cuts'' that create new zones.
For example, the name servers for ``example.com.'' can delegate the authority of ``test.example.com.'' to some other name servers.

The root of this tree-like structure is called DNS root zone, which has the label `.' (dot). 
The servers responsible for answering queries in this zone are called root name servers.
There are 13 root name servers as of July 2020, controlled by various organizations, all under the oversight of Internet Assigned Numbers Authority (IANA).

When a DNS client generates a DNS query asking for an IP address, the local DNS server responds after looking into its cache.
It does not find the answer within cache memory, it forwards the DNS query to recursive DNS resolver which tracks down the DNS record with repetitive DNS queries to root name servers, Top Level Domain (TLD) name servers and authoritative name servers until it gets the target authoritative answer\cite{rfc1034}.
Figure 1 shows the working of the domain name system with sequence number marked on the direction of flow.
\begin{figure}[H]
    \centering
   \includegraphics[width=\textwidth]{images/DNS.jpg}
      \caption{Domain Name System}
 \end{figure}
\section{DNS Vulnerabilities}
\label{sec:dns-vulnerability}

To have a thorough view of DNS security concerns, we examine both attacks that target DNS infrastructure and individual users using DNS, and attacks where DNS protocol is itself not targeted by malicious actors but is abused in the process of attack.
It is worth noting that even though the academic literature on DNS security regards each of these attacks separately, in real life cyberattacks may use a combination of DNS vulnerabilities to create attacks based on their motives, abilities and the situation in hand.

There are multiple ways of categorizing cyberattacks related to DNS protocol.
In this section we categorize these attacks by the type of malicious activity they incorporate.

\subsection{DNS Forgery}
\label{subsec:dns-forgery}
We define DNS Forgery as any activity that is done with the goal of introducing unauthorized changes to the DNS responses.
Some sources call these activities DNS Poisoning, but we found DNS Forgery to be a better umbrella term, since many use DNS Poisoning to refer just to DNS Cache Poisoning.
These activities include DNS Hijacking, DNS Spoofing (aka. DNS Cache Poisoning), DNS Redirection, and DNS Authoritative Poisoning.

\subsubsection{DNS Hijacking}
This type of activity involves a DNS server responding with fake DNS information.
Even though the same term has been used for what we call DNS Redirection in this work, we use DNS Hijacking strictly to refer the act of sending fake DNS responses by DNS servers.
In other words, we don’t consider attacks that change the DNS resolver used by user as DNS Hijacking (even though they may also incorporate DNS Hijacking).
Non-malicious uses of DNS Hijacking include redirecting users to captive portals (used widely in open wireless networks), censorship by governments, blocking malicious websites, and displaying customized error pages (sometimes including ads) instead of responding with NXDOMAIN. It can also be incorporated by rogue DNS servers to redirect users to malicious websites such as the ones used for phishing.

\subsubsection{DNS Spoofing (aka. DNS Cache Poisoning)}
This attack works by introducing forged DNS information into the cache of DNS resolvers, causing the resolver to mistakenly send potentially malicious responses to the queries made by users.
To produce such an effect, the malicious actor should make the DNS resolver believe it had received a valid response to a query it made.
To do this they should send forged DNS responses to the DNS requests made by resolver, with the same query ID and port number as the DNS request packet.
The malicious actor may have the necessary access to the network for performing packet interceptions, allowing them to look at the DNS requests made by resolver (and their port number and query ID).
Otherwise they would have to guess those values, so that their responses won’t get dropped by the resolver.
In addition to the main attack that involves poisoning a single DNS entry (domain), a variation of this attack mostly referred to as \textit{Kaminskey Attack} is able to poison a whole zone (a domain and all of its subdomains).

% References:
% https://www.cs.cornell.edu/~shmat/shmat_securecomm10.pdf
% http://unixwiz.net/techtips/iguide-kaminsky-dns-vuln.html


\subsubsection{DNS Redirection}
In this type of attack, that is also known as ``Resolver Redirection Attack'', the goal of the attacker is to change the default DNS resolver of the user to a name server controlled by them.
This is usually done by means of malware or by changing DNS resolver settings on vulnerable network infrastructure, such as routers with default passwords that are accessible through the network.
This change will let the attacker control all of DNS responses, so that they could collect information and/or possibly redirecting users to malicious web pages.
What makes this attack especially dangerous is that the user does not get any indication of the attack.
By hijacking the DNS responses, the attacker can use DNS redirection to enable more sophisticated attacks, such as phishing, with minimum risk of detection. 

%todo: the book "DNS Security Management" pages 73 - subsection "Resolver Redirection Attacks"

\subsubsection{DNS Authoritative Poisoning}
This attack focuses on changing DNS records on the DNS authoritative servers.
While this change would be harder than changing resolver settings of users, it has the advantage of affecting all of the users at once.
To perform this type of attack, the attacker would need to focus on the vulnerabilities in the implementation of DNS name servers.
Such vulnerabilities may give enough access to the attacker to either change the records or redirect part of the DNS zone to another name server controlled by the attacker.
%todo: the book "DNS Security Management" pages 71 - subsection "Authoritative poisoning"

\subsection{Covert DNS Channels}\label{subsec:covert-dns-channels}
DNS tunneling is a method of using DNS protocol as a mean of encapsulating data transmission between a client and a server.
The data is coded within parts of an otherwise normal DNS request and the server (which is designed to understand these requests) may or may not return with some data encoded in the corresponding DNS response.

There can be different motives for using DNS as a means of data transmission.
Most importantly, many firewalls don’t examine the contents and frequency of DNS packets, which makes it easier for malware to go unnoticed.
Secondly, since the packet will rich the modified nameservers over several hops through DNS recursion, the communication is harder to detect and block, using common methods such as IP blacklisting.
Many malwares have already used this method to exfiltrate data, or to create a communication channel with their command-and-control servers.
This helps attackers to take some degrees of control over the infected devices, and potentially using them to orchestrate attacks on other targets, for example launching DDoS attacks on online services.

The outgoing data is encoded with a special encoding that only produce outputs valid for a subdomain.
For example, variants of base32 can be used.
Then a DNS packet is created using the encoded data as the subdomain of an address that is going to be resolved.
By sending several DNS requests, each containing less than 253 bytes of the data (the maximum length of a full domain name), the malware can send varying amount of data to the attacker.

The incoming data is usually encoded with base64 or similar encodings and put into a TXT record of a DNS response.
This data may be used to control the infected device.

There are multiple ways to detect and shut down DNS tunnels which usually try to investigate the domain names used in DNS requests.
Different approaches have been used to detect anomalous domain names, such as using statistical models.
Frequency of DNS resolutions, length of the subdomains, and the usage of TXT records are also variables that can help detect DNS tunnels.
Blocking DNS tunnels may be achieved through domain blacklisting, IP blacklisting, and dropping DNS packets that are thought to be malicious in nature.

DoH as a new protocol for resolving domain names, have already been criticized by many security researchers for making DNS tunnels harder to detect and mitigate.
There are several ways DoH can helps malwares with their DNS tunnels.
This list includes some of these concerns:
\begin{itemize}
    \item \textbf{Encryption of DNS requests and responses:}
    Since the DoH wraps the DNS traffic in HTTPS, the DNS traffic is unavailable to the network infrastructure between the client (malware) and server (DoH server).
    This effectively makes detection methods that relied on examining the DNS packets obsolete for the firewalls.
    \item \textbf{Hiding the number of requests and responses:}
    HTTP/2 is the minimum version of HTTP that DoH standard (rfc8484) recommends for using with DoH. 
    Malware can utilize this HTTP/2 connection, to send several DoH request, without creating a separate connection (or packet) for each request.
    The same also applies to the responses that DoH server is sending to the malware.
    Through this method, malware can hide the frequency of their DNS resolution, further reducing the number of methods that can be used to detect DNS tunnels.
    \item \textbf{Server push:} 
    C2 servers may also be able to utilize server push, a feature of HTTP/2 that is also part of DoH standard.
    Using server push, servers can send DNS responses to clients, without an explicit DNS request from the clients.
    How this can be achieved, is dependent on DoH server implementation, and might not be available using current DoH servers.
\end{itemize}

\subsection{DNS Rebinding}\label{subsec:dns-rebinding}

% TODO: https://research.nccgroup.com/2020/03/30/impact-of-dns-over-https-doh-on-dns-rebinding-attacks/

% TODO: https://github.com/nccgroup/singularity

\subsection{Network Reconnaissance}\label{subsec:network-recon}



\subsubsection{Query Sniffing}
%todo: the book "DNS Security Management" pages 75 - subsection "Network Reconnaissance"
\subsubsection{Wildcard Queries}
%todo: the book "DNS Security Management" pages 75 - subsection "Network Reconnaissance"
\subsubsection{DNS Zone Transfers}
% page 5 of http://www.infosecwriters.com/text_resources/pdf/dns-security-survey.pdf

\subsection{Denial of Service}\label{subsec:denial-of-service}
There are several types of flood attacks that can cripple DNS servers.
The main idea is to flood a DNS server (either recursive or authoritative, or both) with DNS requests that are crafted to use crucial resources such as CPU, memory, and cache space.
If effective, this attack can slow down the target, thus preventing the resolution of legitimate DNS requests.

DNS servers can mitigate this kind of attacks by several methods, including:
\begin{itemize}
    \item Preventing cache pollution with better cache management, such as not keeping NXDOMAIN responses.
    \item Detecting malicious clients with the number of NXDOMAIN requests and blacklisting them.
    \item Blacklisting hosts that are slow to respond.
\end{itemize}

% DoH usage can affect these attack strategies in many ways.
% Floods that target DoH servers, may be more effective in comparison with their DNS counterpart.
% DoH servers keep an open HTTP/2 connection with their clients that also have the overhead of HTTPS handshaking.
% Attackers can create special malwares that maximizes this overhead on DoH servers, thus making DoH flood attacks more dangerous that DNS flood attacks.

\subsubsection{NXDOMAIN attack}
In the NXDOMAIN variant, attacker uses domain names that don’t exist.
These prompts the recursive server to do DNS recursion.
By flooding the server with these requests, resources such as CPU, and memory get exhausted, and the server slows down.
Depending on the implementation of the DNS server, it can also fill up the DNS cache with NXDOMAIN responses and flush out other cache entries that are legitimate.
This process is called cache pollution, and it forces the DNS server to do more recursions (since the number of cache hits go down drastically in this state).

\subsubsection{Random subdomains attack}
In the Random Subdomain variant, attacker uses addresses that are random subdomains of a legitimate domain.
Like the NXDOMAIN attack, the recursive server still needs to do recursion and experiences the load of the flood of DNS requests. But the flood also incapacitates the authoritative servers of the domain used, by the same mechanism.

where DoH server itself is not the target of flood attack, there are advantages for the attacker in the Random Subdomain variant of the attack.
Since the traffic is funneled through the DoH server, the DNS request would be stripped of data that can identify sources of attack.
So, the target server cannot detect and blacklist malicious clients.
The attack can be mitigated by dropping all packets from the DoH server, but if the usage of DoH instead of DNS is widespread, this means legitimate DNS traffic is also dropped, denying normal users from using the DNS server.

\subsubsection{Phantom domain attack}
In the Phantom Domain variant, attacker sets up special name servers that are designed to be slow, or unresponsive. The flood will target the recursive server with requests that should be resolved using the special name server (for example, using random subdomains of the domain of that server).
The special name server ensures that the recursive server waste a substantial amount of resources for each request.
By flooding the name server with these time-wasting requests, the attacker wishes to exhaust the available resources on the victim's name server, eventually making it impossible for legitimate queries to go through\cite{dooley2017dns}.

\subsubsection{Reflection/Amplification attack}
DNS amplification/reflection attack abuses the connection-less nature of DNS to spoof the sender IP of a dns query, sending the results of all queries he/she makes from a wide range of clients, to the victim's server.
The attacker also may amplify this responses, by creating DNS queries with much larger responses that the query packet size.
This flood of packets may cripple the victim's local network, rendering the victim's server unresponsive.
Several mitigation techniques have been proposed for this problem\cite{di2011protecting,huistra2013detecting,rozekrans2013defending}, but since this issue stems from the original design of DNS protocol, these attacks cannot be completely ruled out.
A recent variant of this attack, called NXNSAttack, abuses the limitations in DNS server implementations to amplify and reflect seemingly legitimate DNS packets to the target servers\cite{shafir2020nxnsattack}.
% Todo: https://www.delaat.net/rp/2012-2013/p29/report.pdf

\section{DNS Security Extensions}
\subsection{DNSSEC}
\subsection{DNS-over-Encryption}
\section{DNS-over-HTTPS}
???An introduction about DoH and related history....???

\subsection{Available Configurations}
???You need to talk about three different available solutions to implement the DoH (Use the LR of the first paper)???

\subsection{DoH Security Concerns and Remaining Vulnerabilities}
While there has been many studies on applicability and performance aspects of DoH\cite{bottger2019empirical, hounsel2019analyzing, wijenbergh2019performance}, studies on the security aspects of DoH still remains limited.
Two objectives seems to have taken the spotlight in DoH security: 1) Privacy and 2) Data Exfiltration.

Siby et al. challenge the privacy of DoH protocol, showing that it's technically possible to defer private information about one's web browsing activity by capturing and analyzing their DoH traffic\cite{siby2018dns, sibyencrypted}.
Other authors also studied DoH privacy aspects, with varying opinions\cite{borgolte2019dns, lu2019end}.
This is because while DoH encrypts the traffic between hops, mitigating man-in-the-middle attacks, widespread use of DoH may result in concentration of queries in a handful of DoH service providers.
By collecting name resolution information, these service providers may be able to identify users and target them with ads or sell their information.

Data exfiltration is another issue with DoH protocol.
DNS tunnels has been widely used by malware to covertly transfer data in and out of networks\cite{nussbaum2009robust}.
DoH protocol can furthermore encrypt DNS traffic, making DoH tunnels virtually undetectable.
This will pose a great danger to enterprise networks\cite{kumari2020sac109, bumanglag2020impact}.

\section{Concluding Remarks}


% In this section, I first outline security vulnerabilities and attacks in DNS protocol and then follow up with the existing literature on the security of DoH protocol.

% \section{DNS Vulnerabilities and Attacks}
% \label{sec:dns-security}

% One of the ways that DNS can have malicious usage is DNS tunneling.
% DNS tunneling is defined as misusing DNS protocol, to communicate data other than those related to the name resolution, for example encapsulating other protocols, or exfiltrating data from a system.
% By using DNS tunnels, the perpetrator hides the real destination of the connection, bypasses firewalls that are mostly indifferent about DNS traffic, and makes detection of the malicious traffic harder.
% Some malwares have used DNS tunnels to communicate with their C2\footnote{Command and Control} servers.

% Since DNS tunnels may employ different methods to hide the data they are exchanging, they can't be trivially detected.
% Detection methods can use different properties of DNS traffic to find DNS tunnels, utilizing both traffic analysis (without inspecting contents of packets) and deep packet inspection\cite{buczak,nadler,engel}.

% There are a variety of DNS DDoS\footnote{Distributed denial of service} attacks that may target the recursive resolvers and/or the authoritative servers\cite{dooley2017dns}.
% These attacks include DNS amplification/reflection attack, NXDOMAIN attack, phantom domain attack, and random subdomain attack\footnote{Also known as DNS water torture attack}.

% \section{Current studies on DoH security}
% \label{sec:doh-security}